import '@tarojs/async-await'

import Taro, { Component } from '@tarojs/taro'
import Special from './pages/index/special'

import api from './api'

import './fontawesome.css'
import './app.less'

class App extends Component {
  config = {
    pages: [
      'pages/index/special',
      'pages/order/list',
      'pages/order/detail',
      'pages/order/success',
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fafafa',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      position: 'top',
      list: [{
        pagePath: 'pages/index/special',
        text: '每周特卖',
      }, {
        pagePath: 'pages/order/list',
        text: '我的订单',
      }]
    }
  }

  async checkLogin() {
    try {
      const token = Taro.getStorageSync('token')
      if (!token) {
        console.log('request account from server')
        const { code } = await Taro.login()
        if (code) {
          await api.login(code)
        }
      } else {
        console.log('already logined.')
      }
    } catch (e) {
      console.warn(e)
    }
  }

  async componentDidMount () {
    this.checkLogin()
  }

  componentDidShow () {}

  componentDidHide () {}

  componentCatchError () {}

  render () {
    return (
      <Special />
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
