import Taro, { Component } from '@tarojs/taro'

import { View, Text, Image } from '@tarojs/components'

import './common.less'
import './order-card.less'

export default class SmallOrderCard extends Component {

  formatDate(dateStr) {
    const date = new Date(dateStr)
    return `${date.toLocaleDateString('zh-CN')} ${date.toLocaleTimeString()}`
  }

  formatStatus(status) {
    switch (status) {
    case 0: return '待付款'
    case 1: return '待发货'
    case 2: return '已发货'
    case 3: return '已收货'
    case 4: return '已取消'
    }
  }

  render() {
    return (
      <View style='margin-bottom: 24rpx'>
        <View className='order_card_side_info'>
          <Text>下单时间：{this.formatDate(this.props.order.createdAt)}</Text>
          <Text className='order_card_hightlight'>{this.formatStatus(this.props.order.status)}</Text>
        </View>
        <View className='order_card_product'>
          <View className='order_card_product_thumbnail_c'>
            <Image
              src={this.props.order.product.thumbnail}
              className='order_card_product_thumbnail'
            />
          </View>
          <View className='order_card_product_detail'>
            <Text>{this.props.order.product.name}</Text>
            <Text>￥ {this.props.order.product.price}</Text>
          </View>
        </View>
        <View>
          <View className='order_card_pay_price text_small text_second'>
            <Text>实付：</Text>
            <Text className='order_card_hightlight'>￥{this.props.order.product.price}</Text>
          </View>
        </View>
      </View>
    )
  }
}
