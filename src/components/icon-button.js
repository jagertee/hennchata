import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import FontAwesomeIcon from './fontawesome-icon'

import './icon-button.less'

export default class IconButton extends Component {

  render () {
    return (
      <View className='icon_button_container'>
        <FontAwesomeIcon name={this.props.iconName} className='icon_button_icon' />
        <Text className='icon_button_text'>{this.props.text}</Text>
      </View>
    )
  }
}
