import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import FontAwesomeIcon from './fontawesome-icon'

import './list-item.less'

export default class ListItem extends Component {
  render () {
    return (
      <View className='list_item_row'>
        <Text>{this.props.text}</Text>
        <FontAwesomeIcon name='angle-right' />
      </View>
    )
  }
}
