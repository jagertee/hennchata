import Taro, { Component } from '@tarojs/taro'
import { Text } from '@tarojs/components'

import './fontawesome-icon.less'

export default class FontAwesomeIcon extends Component {

  render () {
    let c = 'icon fa fa-' + this.props.name
    if (this.props.className) {
      c = this.props.className + ' ' + c
    }
    if (this.props.size === 'big') {
      c = 'icon-big ' + c
    }
    return <Text className={c} />
  }
}
