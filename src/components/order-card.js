import Taro, { Component } from '@tarojs/taro'

import { View, Text, Image } from '@tarojs/components'

import './order-card.less'

export default class OrderCard extends Component {

  formatDate(dateStr) {
    const date = new Date(dateStr)
      return `${date.toLocaleDateString('zh-CN')} ${date.toLocaleTimeString()}`
    }

  formatStatus(status) {
    switch (status) {
    case 0: return '待付款'
    case 1: return '待发货'
    case 2: return '已发货'
    case 3: return '已收货'
    case 4: return '已取消'
    }
  }

  render() {
    return (
      <View>
        <View className='order_card_side_info'>
          <Text>解忧珠宝屋</Text>
          <Text className='order_card_hightlight'>
            {this.formatStatus(this.props.order.status)}
          </Text>
        </View>
        <View className='order_card_product'>
          <View className='order_card_product_thumbnail_c'>
            <Image
              src={this.props.order.product.thumbnail}
              className='order_card_product_thumbnail'
            />
          </View>
          <View className='order_card_product_detail'>
            <Text>{this.props.order.product.name}</Text>
            <Text>￥ {this.props.order.product.price}</Text>
          </View>
        </View>
        <View>
          <View className='order_card_side_info'>
            <Text>支付方式： </Text>
            <Text>微信支付</Text>
          </View>
          <View className='order_card_side_info order_card_bottom_border'>
            <Text>运费：</Text>
            <Text>免邮</Text>
          </View>
          <View className='order_card_pay_price'>
            <Text>实付：</Text>
            <Text className='order_card_hightlight'>￥{this.props.order.product.price}</Text>
          </View>
        </View>
      </View>
    )
  }
}
