import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button, Image } from '@tarojs/components'

import ListItem from '../../components/list-item'
import IconButton from '../../components/icon-button'

import './me.less'

export default class OrderPage extends Component {
  config = {
    navigationBarTitleText: '我的订单'
  }

  async componentDidMount () {
  }

  render () {
    return (
      <View className='order_page_container'>
        <View className='orders'>
          <ListItem text='全部订单' />
          <View className='order_categories'>
            <IconButton iconName='wallet' text='待付款' />
            <IconButton iconName='address-card' text='待发货' />
            <IconButton iconName='truck' text='待收货' />
            <IconButton iconName='file-signature' text='已签收' />
          </View>
          <View className='divider' />
          <ListItem text='联系客服' />
        </View>
      </View>
    )
  }
}
