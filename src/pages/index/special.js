import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button, Image } from '@tarojs/components'
import './special.less'

import api from '../../api'

export default class Special extends Component {
  config = {
    navigationBarTitleText: '首页'
  }

  state = {
    product: null,
  }

  async componentDidMount () {
    const result = await api.getOnSaleProduct()
    if (result.error) {
      Taro.showToast({
        icon: "none",
        title: "error fetching data"
      })
    } else {
      this.setState({
        product: result,
      })
      Taro.setNavigationBarTitle({
        title: result.name
      })
    }
  }

  confirmAdress (addr) {
    const addressStr =
          "您的收货地址是\r\n" +
          addr.provinceName + addr.cityName +
          addr.countyName + addr.detailInfo +
          addr.userName + addr.postalCode
    return Taro.showModal({
      title: '收货地址确认',
      content: addressStr,
    })
  }

  onOrderClick = async () => {
    // TODO permission check
    try {
      const addr = await Taro.chooseAddress()
      Taro.showLoading({
        title: '正在创建订单',
        mask: true,
      })
      console.log(this.state.product)
      const order = await api.createOrder({
        productId: this.state.product.id,
        address: addr,
      })
      Taro.navigateTo({
        url: '/pages/order/detail?orderId=' + order.id
      })
    } catch (e) {
      if (e.errMsg && e.errMsg.includes('deny')) {
        Taro.showToast({
          icon: 'none',
          title: '无法获取收货地址'
        })
      } else {
        Taro.showToast({
          icon: 'none',
          title: '暂时无法创建订单，请稍后再试'
        })
      }
    } finally {
      Taro.hideLoading()
    }
  }

  render () {
    return (
      <View className='page_container'>
        <View className='page_content'>
          { this.state.product &&
            this.state.product.images &&
            <View>
                {this.state.product.images.map(
                  (url) => <Image key={url} src={url} mode='widthFix' className='image' />
                )}
            </View>}
        </View>
        <View className='page_bottom_bar'>
          <Text className='price-label'>{this.state.product.price}元</Text>
          <View className='.bottom-button-group'>
            <Button plain size='mini'
              type='primary'
              className='clear-button'
              openType='contact'
            >
              联系客服
            </Button>
            <Button
              type='primary'
              className='clear-button'
              onClick={this.onOrderClick}
            >
              立即购买
            </Button>
          </View>
        </View>
      </View>
    )
  }
}
