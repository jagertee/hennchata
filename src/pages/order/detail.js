import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'

import FontAwesomeIcon from '../../components/fontawesome-icon'
import OrderCard from '../../components/order-card'

import api from '../../api'

import '../../app.less'
import './detail.less'

export default class DetailPage extends Component {

  config = {
    navigationBarTitleText: '订单详情',
  }

  async componentDidMount () {
    const {orderId} = this.$router.params

    // TODO handle error
    if (!orderId) {
      Taro.navigateBack()
    } else {
      const order = await api.getOrder(orderId)
      this.setState({ order })
    }
  }

  onCancelClick = async () => {
    if (this.state.order) {
      Taro.showLoading({
        title: '正在取消订单'
      })
      try {
        console.log(this.state.order.id)
        await api.cancelOrder(this.state.order.id)
        Taro.switchTab({
          url: '/pages/order/list'
        })
      } catch (e) {
        Taro.showToast({
          icon: 'none',
          title: '暂时无法取消订单，请稍后再试'
        })
      } finally {
        Taro.hideLoading()
      }
    }
  }

  onPayClick = async () => {
    // TODO pay and pay success
    if (this.state.order) {
      Taro.showLoading({
        title: '正在等待支付结果'
      })
      try {
        await api.payOrder(this.state.order.id)
        Taro.navigateTo({
          url: '/pages/order/success'
        })
      } catch (e) {
        Taro.showToast({
          icon: 'none',
          title: '支付失败，请稍后再试'
        })
      } finally {
        Taro.hideLoading()
      }
    }
  }

  render () {
    if (!this.state.order) {
      return null
    }
    const addr = this.state.order.address
    return (
      <View className='page_container'>
        <View className='page_content'>
          <View className='order_detail_address_bar'>
            <FontAwesomeIcon name='map-marker-alt' size='big' />
            <View className='order_detail_address_detail'>
              <View className='order_detail_address_name'>
                <Text>收件人：{addr.userName}</Text>
                <Text>{addr.telNumber}</Text>
              </View>
              <Text>{addr.provinceName} {addr.cityName} {addr.countyName} {addr.detailInfo}</Text>
            </View>
          </View>
          <View className='order_detail_card'>
            <OrderCard order={this.state.order} />
          </View>
        </View>
        <View className='page_bottom_bar'>
          <Button className='page_button' size='mini' openType='contact'>
            联系客服
          </Button>
          {this.state.order.status === 0 &&
            <Button className='page_button' size='mini' onClick={this.onCancelClick}>
              取消订单
            </Button>}
          {this.state.order.status === 0 &&
            <Button className='page_button' type='primary' size='mini' onClick={this.onPayClick}>
              付款
            </Button>}

        </View>
      </View>
    )
  }
}
