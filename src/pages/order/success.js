import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'

import FontAwesomeIcon from '../../components/fontawesome-icon'

import './success.less'

export default class OrderSuccessPage extends Component {
  config = {
    navigationBarTextStyle: '付款成功'
  }

  onNavClick = () => {
    Taro.switchTab({
      url: '/pages/order/list'
    })
  }

  render() {
    return (
      <View className='success_page_container'>
        <View>
          <FontAwesomeIcon name='smile' />
          <Text>付款成功，随后将为您安排发货</Text>
        </View>
        <Button type='primary' size='mini' onClick={this.onNavClick}>
          点击查看我的订单
        </Button>
      </View>
    )
  }
}
