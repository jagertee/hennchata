import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'

import SmallOrderCard from '../../components/order-card-small'
import FontAwesomeIcon from '../../components/fontawesome-icon'

import api from '../../api'

import './list.less'


export default class OrderList extends Component {

  config = {
    navigationBarTitleText: '我的订单'
  }

  async componentDidShow() {
    const orders = await api.getOrders()
    this.setState({ orders })
  }

  onOrderClick(order) {
    console.log(order)
    console.log('/pages/order/detail?orderId=' + order.id)
    Taro.navigateTo({
      url: '/pages/order/detail?orderId=' + order.id,
    })
  }

  render() {
    if (!this.state.orders || !this.state.orders.length) {
      return (
        <View className='order_list_empty'>
          <FontAwesomeIcon name='smile' />
          <Text>你还没有下单哦</Text>
        </View>
      )
    }
    return (
      <View>
        {this.state.orders.map(order => {
          return (
            <View key={order.id} onClick={this.onOrderClick.bind(this, this.order)}>
              <SmallOrderCard order={order} />
            </View>
          )
        })}
      </View>
    )
  }
}
