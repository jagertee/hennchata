import Taro from '@tarojs/taro'

// const BASE_URL = 'http://localhost:3000/v1'
const BASE_URL = 'https://panama.luois.ninja/v1'

async function fetch(endpoint, options={}) {
  let headers = {}
  let url = BASE_URL
  if (options.authRequired) {
    const token = Taro.getStorageSync('token')
    headers['Authorization'] = token
    url = url + '/u'
  }

  url = url + endpoint
  if (options.params) {
    let paramStr = '?'
    for (const key of Object.keys(options.params)) {
      paramStr += `${key}=${options.params[key]}&`
    }
    url += paramStr
  }

  const resp = await Taro.request({
    url: url,
    method: options.method || 'GET',
    header: headers,
    data: options.body,
  })
  if (resp.statusCode === 401) {
    Taro.removeStorage({
      key: 'token',
    })
  }
  if (resp.statusCode != 200) {
    throw new Error(resp.errMsg)
  }
  return resp.data
}

async function login(code) {
  const data = await fetch('/login', {
    method: 'POST',
    body: { code },
  })
  await Taro.setStorage({
    key: 'token',
    data: data.token,
  })
}

async function createOrder({productId, address}) {
  return fetch('/order', {
    authRequired: true,
    method: 'POST',
    body: { productId, address }
  })
}

// FIXME test only
async function payOrder(orderId) {
  return fetch('/order/pay', {
    authRequired: true,
    method: 'POST',
    body: { orderId },
  })
}

async function cancelOrder(orderId) {
  return fetch('/order/cancel', {
    authRequired: true,
    method: 'POST',
    body: { orderId },
  })
}

async function getOrder(orderId) {
  return fetch('/order', {
    authRequired: true,
    params: { orderId },
  })
}

async function getOrders() {
  return fetch('/orders', {
    authRequired: true,
  })
}

async function getOnSaleProduct() {
  return fetch('/product/onsale')
}

export default {
  login,
  getOrder,
  getOrders,
  getOnSaleProduct,

  createOrder,
  payOrder,
  cancelOrder,
}
